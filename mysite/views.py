from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

class HomeView(TemplateView):
	template_name = "home.html"

class RegisterView(TemplateView):
	template_name = "registration/register.html"

	def dispatch(self, request, *args, **kwargs):
		context={}
		if  request.method == 'POST':
			message = None
			username=request.POST.get("username")
			fullname=request.POST.get("fullname")
			email=request.POST.get("email")
			password=request.POST.get("password")
			password2=request.POST.get("password2")
			if len(password) > 8 and password==password2:
				user=User.objects.create_user(username, email, password)
				user.first_name = fullname
				user.save()
				message = "User registered soccessfilly!"
			else:
				message = "Password must have more than 8 letters"
			context['message'] = message
		return render(request, self.template_name, context)

class ProfilePage(TemplateView):
	template_name = "registration/profile.html"

def logout(request):
	auth.logout(request)
	return redirect("/")
